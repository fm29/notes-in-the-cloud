import {Router} from "express";
import NoteController from "../controller/note.controller";

const router = Router();
const noteController = new NoteController();

router.post('/api/users/:uid/notes', noteController.createNote.bind(noteController));
router.delete('/api/users/:uid/notes/:noteId', noteController.deleteNote.bind(noteController));
router.delete('/api/users/:uid/notes', noteController.deleteUserNotes.bind(noteController));
router.patch('/api/users/:uid/notes/:noteId', noteController.updateNote.bind(noteController));
router.get('/api/notes/:noteId', noteController.getNote.bind(noteController));
router.put('/api/users/:uid/notes/:noteId/authz', noteController.addNoteAuthorizations.bind(noteController));
router.get('/api/search/notes',noteController.searchNotes.bind(noteController))

export {router as noteRouter};
