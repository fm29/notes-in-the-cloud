import {Router} from "express";
import UserController from "../controller/user.controller";

const userController = new UserController();
const router = Router();

router.post('/api/users/register', userController.registerUser.bind(userController));
router.delete('/api/users/:uid', userController.unregisterUser.bind(userController));
router.patch('/api/users/:uid', userController.updateUser.bind(userController));
router.get('/api/users/:uid', userController.getUserById.bind(userController));
router.get('/api/users/:uid/notes', userController.getUserNotes.bind(userController));
router.get('/api/users/:uid/sharedNotes', userController.getUserSharedNotes.bind(userController));
router.get('/api/users/:uid/todo', userController.getUserToDoList.bind(userController));
router.put('/api/users/:uid/todo', userController.updateUserToDoList.bind(userController));

export {router as userRouter};
