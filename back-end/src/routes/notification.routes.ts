import {Router} from "express";
import NotificationController from "../controller/notification.controller";

const router = Router();
const notificationController = new NotificationController();

router.post('/api/users/:uid/notifications', notificationController.createNotification.bind(notificationController));
router.delete('/api/users/:uid/notifications/:notificationId', notificationController.deleteNotification.bind(notificationController));
router.delete('/api/users/:uid/notifications', notificationController.deleteUserNotifications.bind(notificationController));
router.patch('/api/users/:uid/notifications/:notificationId', notificationController.updateNotification.bind(notificationController));
router.get('/api/users/:uid/notifications/:notificationId', notificationController.getNotification.bind(notificationController));
router.get('/api/users/:uid/notifications', notificationController.getUserNotifications.bind(notificationController));


export {router as notificationRouter};
