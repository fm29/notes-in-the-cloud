import {Router} from "express";
import AuthnController from "../controller/authn.controller";

const router = Router();
const authnController = new AuthnController();

router.post('/api/users/authenticate', authnController.login.bind(authnController));
router.post('/api/users/:uid/logout', authnController.logout.bind(authnController));

export {router as authnRouter};
