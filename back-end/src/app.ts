import * as express from "express";
import * as bodyParser from "body-parser";
import * as cors from "cors";

import {userRouter} from "./routes/user.routes";
import {authnRouter} from "./routes/authn.routes";
import mongoose from "mongoose";
import {noteRouter} from "./routes/note.routes";
import {notificationRouter} from "./routes/notification.routes";

class App {

  public app: express.Application;

  private corsOptions: cors.CorsOptions = {
    origin: [
      'http://127.0.0.1:4200',
      'http://localhost:4200'
    ]
  };

  private dbURI = 'mongodb+srv://admin:MZHj3b2i23KJt0q2@webtechdb.zano3.mongodb.net';
  private dbName = 'WebDB'

  constructor() {
    this.app = express();
    this.config();
  }

  private config(): void {
    this.connectToDb();
    this.app.use(cors(this.corsOptions));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({extended: false}));
    this.app.use(authnRouter);
    this.app.use(userRouter);
    this.app.use(noteRouter);
    this.app.use(notificationRouter);
  }

  private connectToDb(): void {
    mongoose.connect(this.dbURI, {
      dbName: this.dbName
    });
  }

}

export default new App().app;
