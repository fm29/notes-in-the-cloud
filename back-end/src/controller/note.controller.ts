import {Request, Response} from "express";
import {Note} from "../model/note.model";
import NoteService from "../service/note.service";
import AuthnUtil from "../util/authnUtil";
import UserService from "../service/user.service";


export default class NoteController {

  private noteService: NoteService;
  private userService: UserService;

  constructor() {
    this.noteService = NoteService.Instance;
    this.userService = UserService.Instance;
  }

  public async createNote(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      req.body.uid = req.params.uid;
      const newNote = new Note(req.body);
      await this.noteService.createNote(newNote);
      res.status(201).json(newNote);
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async deleteNote(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const noteId = req.params.noteId;
      const uid = req.params.uid;
      await this.noteService.deleteNote(uid, noteId);
      res.status(204).json({message: "Note successfully deleted"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async deleteUserNotes(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const userId = req.params.uid;
      await this.noteService.deleteAllNotesByUid(userId);
      res.status(204).json({message: "Notes successfully deleted"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async updateNote(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const note = new Note(req.body);
      await this.noteService.updateNote(req.params.uid, req.params.noteId, note);
      res.status(200).json({message: "Note successfully updated"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getNote(req: Request, res: Response) {
    try {
      const noteId = req.params.noteId;
      const authToken = AuthnUtil.getTokenFromAuthHeader(req.headers.authorization);
      const authenticatedUserId = await AuthnUtil.getAuthenticatedUser(authToken);
      const authenticatedUser = await this.userService.findById(authenticatedUserId);
      const usersAuthorizedToViewNote = await this.noteService.getAuthorizedUsers(noteId);
      if (usersAuthorizedToViewNote.uid != authenticatedUserId && usersAuthorizedToViewNote.authorizedUsers.indexOf(authenticatedUser?.username) == -1) {
        res.status(403).json({error: "You do not have permission to access the resource"});
        return;
      }

      const note = await this.noteService.findById(noteId);
      if (!!note && usersAuthorizedToViewNote.uid != authenticatedUserId) {
        note.authorizedUsers = undefined;
      }
      const response = {
        note: note,
        canEdit: (authenticatedUserId == usersAuthorizedToViewNote.uid)
      };
      !!note
        ? res.status(200).json(response)
        : res.status(404).json({error: "Note not found"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async addNoteAuthorizations(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const noteId = req.params.noteId;
      const users: string[] = req.body.users;
      const existingUsers = [];
      for (let i = 0; i < users.length; ++i) {
        const user = (await this.userService.findByUsername(users[i]));
        if (user != null) {
          existingUsers.push(user.username);
        }
      }
      if (existingUsers.length == 0) {
        res.status(404).json({error: "None of the users exist"})
      }
      await this.noteService.addUsersToAuthorizedUsers(noteId, existingUsers);
      res.status(200).json({message: "Authorizations updated with existing users"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async searchNotes(req: Request, res: Response) {
    try {
      const userId: string = await AuthnUtil.getAuthenticatedUser(AuthnUtil.getTokenFromAuthHeader(req.headers.authorization));
      const noteText: string = req.query.text?.toString();
      if (noteText == null) {
        res.send(400).json({error: "Invalid search parameter"});
        return;
      }
      const notes = await this.noteService.findUserNotesByText(userId, noteText);
      res.status(200).json({notes: (notes != null) ? notes : []})
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }
}


