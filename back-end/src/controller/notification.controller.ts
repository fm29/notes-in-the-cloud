import NotificationService from "../service/notification.service";
import {Request, Response} from "express";
import {Notification} from "../model/notification.model";
import AuthnUtil from "../util/authnUtil";


export default class NotificationController {
  private notificationService: NotificationService

  constructor() {
    this.notificationService = NotificationService.Instance;
  }

  public async createNotification(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const newNotification = new Notification(req.body);
      newNotification.uid = req.params.uid;
      await this.notificationService.createNotification(newNotification);
      res.status(200).json(newNotification);
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async deleteNotification(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      await this.notificationService.deleteNotification(req.params.uid, req.params.notificationId);
      res.status(204).json({message: "Notification successfully deleted"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async deleteUserNotifications(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      await this.notificationService.deleteAllNotificationsByUid(req.params.uid);
      res.status(204).json({message: "Notifications successfully deleted"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async updateNotification(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const notification = new Notification(req.body);
      await this.notificationService.updateNotification(req.params.uid, req.params.notificationId, notification);
      res.status(200).json({message: "Notification successfully updated"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getNotification(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const result = await this.notificationService.findByUidAndId(req.params.uid, req.params.notificationId);
      !!result
        ? res.status(200).json(result)
        : res.status(404).json({error: "Notification does not exist"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getUserNotifications(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const notifications = await this.notificationService.findByUid(req.params.uid);
      !!notifications
        ? res.status(200).json(notifications)
        : res.status(404).json({error: "No notifications found"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }
}
