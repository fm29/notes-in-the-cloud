import {Request, Response} from "express";
import {User} from "../model/user.model";
import UserService from "../service/user.service";
import AuthnUtil from "../util/authnUtil";
import AuthnService from "../service/authn.service";
import NoteService from "../service/note.service";
import NotificationService from "../service/notification.service";
import TodoService from "../service/todo.service";

export default class UserController {

  private userService: UserService;
  private noteService: NoteService;
  private authnService: AuthnService;
  private notificationService: NotificationService;
  private toDoService: TodoService;

  constructor() {
    this.userService = UserService.Instance;
    this.authnService = AuthnService.Instance;
    this.noteService = NoteService.Instance;
    this.notificationService = NotificationService.Instance;
    this.toDoService = TodoService.Instance;
  }

  public async registerUser(req: Request, res: Response) {
    try {
      const newUser = new User(req.body);
      const hashAndSalt = await AuthnUtil.hashPassword(req.body.password, null);
      newUser.password = undefined;
      newUser.hashedPassword = hashAndSalt.hashedPassword;
      newUser.salt = hashAndSalt.salt;
      await this.userService.registerUser(newUser);
      await this.toDoService.createUserToDoList(newUser._id);
      const sessionToken: string = AuthnUtil.generateToken();
      await this.authnService.saveToken(sessionToken, newUser._id.toString());
      res.status(201).json({uid: newUser._id.toString(), token: sessionToken});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async unregisterUser(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const userId = req.params.uid;
      await this.noteService.deleteAllNotesByUid(userId);
      await this.userService.unregisterUser(userId);
      await this.notificationService.deleteAllNotificationsByUid(userId);
      await this.noteService.removeUserFromAllAuthorizations(userId);
      await this.toDoService.deleteUserToDoList(userId);
      res.status(204).json({message: "User successfully deleted"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async updateUser(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const userId = req.params.uid;
      const user = new User(req.body);
      const password = req.body.password?.trim();
      if (password != null && password.length != 0) {
        const hashedPasswordAndSalt = await AuthnUtil.hashPassword(req.body.password, null);
        user.hashedPassword = hashedPasswordAndSalt.hashedPassword;
        user.salt = hashedPasswordAndSalt.salt;
      }
      await this.userService.updateUser(userId, user);
      res.status(200).json({message: "User successfully updated"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getUserById(req: Request, res: Response) {
    try {
      const userId = req.params.uid;
      const user = await this.userService.findById(userId);
      user.hashedPassword = undefined;
      user.salt = undefined;
      !!user
        ? res.status(200).json(user)
        : res.status(404).json({error: "No such user"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getUserNotes(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const uid = req.params.uid;
      const notes = await this.noteService.findByUid(uid);
      !!notes
        ? res.status(200).json(notes)
        : res.status(404).json({error: "Notes not found"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getUserSharedNotes(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const uid = req.params.uid;
      const user = await this.userService.findById(uid);
      const notes = await this.noteService.findAllNotesUserIsAuthorizedToView(user?.username);
      notes.map(note => {
        note.authorizedUsers = undefined;
      });
      !!notes
        ? res.status(200).json(notes)
        : res.status(404).json({error: "Shared notes not found"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async getUserToDoList(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const uid = req.params.uid;
      const toDoList = await this.toDoService.getUserToDoList(uid);
      !!toDoList
        ? res.status(200).json(toDoList)
        : res.status(404).json({error: "ToDo List not found"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async updateUserToDoList(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const uid = req.params.uid;
      const toDoList = req.body.toDoList;
      await this.toDoService.updateUserToDoList(uid, toDoList);
      res.status(200).json({message: "ToDo List successfully updated"})
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }
}
