import {Request, Response} from "express";
import UserService from "../service/user.service";
import AuthnService from "../service/authn.service";
import AuthnUtil, {HashedPassword} from "../util/authnUtil";

export default class AuthnController {

  private userService: UserService;
  private authnService: AuthnService;

  constructor() {
    this.userService = UserService.Instance;
    this.authnService = AuthnService.Instance;
  }

  public async login(req: Request, res: Response) {
    try {
      const username: string = req.body.username?.toString();
      const password: string = req.body.password?.toString();
      if (username == undefined || username.length == 0 || password == undefined || password.length == 0) {
        res.status(401).json({error: "Invalid credentials"});
      }
      const user = await this.userService.findByUsername(username)
      if (user == null) {
        res.status(401).json({error: "Invalid credentials"});
        return;
      }
      const hashedPassword: HashedPassword = await AuthnUtil.hashPassword(password, user.salt);
      if (hashedPassword.hashedPassword == user.hashedPassword) {
        const sessionToken: string = AuthnUtil.generateToken();
        await this.authnService.saveToken(sessionToken, user._id.toString());
        res.status(200).json({uid: user._id.toString(), token: sessionToken});
        return;
      }
      res.status(401).json({error: "Error while authenticating"});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }

  public async logout(req: Request, res: Response) {
    try {
      if (!await AuthnUtil.checkForPermissions(req, res)) {
        return;
      }
      const token = AuthnUtil.getTokenFromAuthHeader(req.headers.authorization);
      await this.authnService.deleteToken(token, req.params.uid);
      res.status(200).json({message: "User successfully logged out."});
    } catch (error) {
      console.error(error);
      res.status(400).json({error: error.message});
    }
  }
}
