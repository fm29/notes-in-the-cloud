import Mongoose from "mongoose";
import {Note} from "../model/note.model";

export default class NoteService {

  private static _instance: NoteService;

  private constructor() {
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async createNote(note): Promise<void> {
    try {
      note._id = new Mongoose.Types.ObjectId();
      await Note.create(note);
    } catch (e) {
      console.log(e);
      throw Error('Error while creating new note');
    }
  }

  public async deleteNote(uid: string, noteId: string): Promise<void> {
    try {
      const query = {
        uid: uid,
        _id: new Mongoose.Types.ObjectId(noteId)
      };
      await Note.deleteOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while deleting note');
    }
  }

  public async deleteAllNotesByUid(uid: string): Promise<void> {
    try {
      const query = {uid: uid};
      await Note.deleteMany(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while deleting notes.')
    }
  }

  public async updateNote(uid: string, noteId: string, note): Promise<void> {
    try {
      const query = {
        _id: new Mongoose.Types.ObjectId(noteId),
        uid: uid
      };
      await Note.findOneAndUpdate(query, {
        $set: {
          ...(note.name != null && {name: note.name}),
          ...(note.content != null && {content: note.content})
        }
      }).exec();
    } catch (e) {
      throw Error('Error while updating note.')
    }
  }

  public async findById(noteId: string) {
    try {
      const query = {_id: new Mongoose.Types.ObjectId(noteId)};
      return await Note.findOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving note.')
    }
  }

  public async findByUid(uid: string) {
    try {
      const query = {uid: uid};
      return await Note.find(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving note.')
    }
  }

  public async addUsersToAuthorizedUsers(noteId: string, users: string[]) {
    try {
      const query = {
        _id: new Mongoose.Types.ObjectId(noteId)
      };
      await Note.findOneAndUpdate(query, {
        authorizedUsers: users
      }).exec();
    } catch (e) {
      throw Error('Error while updating note authorizations.')
    }
  }

  public async getAuthorizedUsers(noteId: string): Promise<any> {
    try {
      const query = {_id: new Mongoose.Types.ObjectId(noteId)};
      return (await Note.findOne(query, {uid: 1, authorizedUsers: 1}).exec());
    } catch (e) {
      console.log(e);
      throw Error('User cannot access the resource')
    }
  }

  public async removeUserFromAllAuthorizations(uid: string) {
    try {
      const query = {};
      await Note.updateMany(query, {
        $pull: {authorizedUsers: uid}
      }).exec();
    } catch (e) {
      throw Error('Error while updating note authorizations.')
    }
  }

  public async findAllNotesUserIsAuthorizedToView(username: string) {
    try {
      const query = {authorizedUsers: [username]};
      return await Note.find(query).exec();
    } catch (e) {
      throw Error('Error while retrieving user shared notes.')
    }
  }

  public async findUserNotesByText(uid: string, noteText: string) {
    try {
      const query = {
        $and:
          [{uid: uid},
            {
              $or:
                [
                  {name: {$regex: noteText, $options: "i"}},//i for case insensitive
                  {content: {$regex: noteText, $options: "i"}}
                ]
            }]
      };
      return await Note.find(query).exec();
    } catch (e) {
      throw Error('Error while searching notes.')
    }
  }
}

