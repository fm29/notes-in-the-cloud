import {Token} from "../model/token.model";

export default class AuthnService {

  private static _instance: AuthnService;

  private constructor() {
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async saveToken(token: string, uid: string) {
    try {
      const tokenObject = new Token({
        token: token,
        uid: uid
      });
      await Token.create(tokenObject);
    } catch (e) {
      console.log(e);
      throw Error('Error while saving token.');
    }
  }

  public async deleteToken(token: string, uid: string) {
    try {
      const tokenObject = {
        token: token,
        uid: uid
      };
      await Token.deleteOne(tokenObject).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while deleting token.');
    }
  }

  public async getToken(token: string) {
    try {
      const tokenObject = {token: token}
      return await Token.findOne(tokenObject).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving token.');
    }
  }

}
