import Mongoose from "mongoose";
import {Notification} from "../model/notification.model";

export default class NotificationService {

    private static _instance: NotificationService;

    private constructor() {
    }

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    public async createNotification(notification): Promise<void> {
        try {
            notification._id = new Mongoose.Types.ObjectId();
            await Notification.create(notification);
        } catch (e) {
            console.log(e);
            throw Error('Error while creating new notification');
        }
    }

    public async deleteNotification(uid: string, notificationId: string): Promise<void> {
        try {
            const query = {
                uid: uid,
                _id: new Mongoose.Types.ObjectId(notificationId)
            };
            await Notification.deleteOne(query).exec();
        } catch (e) {
            console.log(e);
            throw Error('Error while deleting notification');
        }
    }

    public async deleteAllNotificationsByUid(uid: string): Promise<void> {
        try {
            const query = {uid: uid};
            await Notification.deleteMany(query).exec();
        } catch (e) {
            console.log(e);
            throw Error('Error while deleting notifications');
        }
    }

    public async updateNotification(uid: string, notificationId: string, notification): Promise<void> {
        try {
            const query = {
                uid: uid,
                _id: new Mongoose.Types.ObjectId(notificationId)
            };
            await Notification.findOneAndUpdate(query, {
                $set: {
                    ...(notification.name != null && {name: notification.name}),
                    ...(notification.message && {message: notification.message}),
                    ...(notification.dueData && {dueData: notification.dueData})
                }
            }).exec();
        } catch (e) {
            console.log(e);
            throw Error('Error while updating notification.')
        }
    }

    public async findByUidAndId(uid: string, notificationId: string) {
        try {
            const query = {_id: new Mongoose.Types.ObjectId(notificationId)};
            return await Notification.findOne(query).exec();
        } catch (e) {
            console.log(e);
            throw Error('Error while retrieving notification.')
        }
    }

    public async findByUid(uid) {
        try {
            const query = {uid: uid};
            return await Notification.find(query).exec();
        } catch (e) {
            console.log(e);
            throw Error('Error while retrieving user.')
        }
    }
}