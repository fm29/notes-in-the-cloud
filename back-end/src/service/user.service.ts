import * as Mongoose from "mongoose";
import {User} from "../model/user.model";

export default class UserService {

  private static _instance: UserService;

  private constructor() {
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async registerUser(user) {
    try {
      user._id = new Mongoose.Types.ObjectId();
      await User.create(user);
    } catch (e) {
      //duplicate key
      if (e.code == 11000) {
        throw Error('Username already taken');
      }
      console.log(e);
      throw Error('Error while registering user.');
    }
  }

  public async unregisterUser(userId: string) {
    try {
      const query = {_id: new Mongoose.Types.ObjectId(userId)};
      await User.deleteOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while deleting user.')
    }
  }

  public async updateUser(userId: string, updatedUser) {
    try {
      const query = {_id: new Mongoose.Types.ObjectId(userId)};
      updatedUser._id = undefined;
      await User.findOneAndUpdate(query, {
        $set: {
          ...(updatedUser.hashedPassword != null && updatedUser.salt != null && {
            hashedPassword: updatedUser.hashedPassword,
            salt: updatedUser.salt
          }),
          ...(updatedUser.email != null && {email: updatedUser.email}),
          ...(updatedUser.firstName != null && {firstName: updatedUser.firstName}),
          ...(updatedUser.lastName != null && {lastName: updatedUser.lastName}),
          ...(updatedUser.gender != null && {gender: updatedUser.gender}),
        }
      }).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while updating user.')
    }
  }

  public async findById(userId: string) {
    try {
      const query = {_id: new Mongoose.Types.ObjectId(userId)};
      return await User.findOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving user.')
    }
  }

  public async findByUsername(username: string) {
    try {
      const query = {username: username};
      return await User.findOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving user.')
    }
  }
}
