import {ToDo} from "../model/todo.model";

export default class TodoService {

  private static _instance: TodoService;

  private constructor() {
  }

  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async createUserToDoList(uid: string) {
    try {
      const toDo = new ToDo({
        uid: uid,
        toDoList: []
      })
      return await ToDo.create(toDo);
    } catch (e) {
      console.log(e);
      throw Error('Error while creating ToDo List.')
    }
  }

  public async getUserToDoList(uid: string) {
    try {
      const query = {uid: uid};
      return await ToDo.findOne(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while retrieving ToDo List.')
    }
  }

  public async updateUserToDoList(uid: string, todoItems) {
    try {
      const query = {uid: uid};
      await ToDo.findOneAndUpdate(query, {toDoItems: todoItems}).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while updating ToDo List.')
    }
  }

  public async deleteUserToDoList(uid: string) {
    try {
      const query = {uid: uid};
      return await ToDo.findOneAndDelete(query).exec();
    } catch (e) {
      console.log(e);
      throw Error('Error while deleting ToDo List.')
    }
  }
}
