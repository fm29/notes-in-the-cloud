import AuthnService from "../service/authn.service";
import {Request, Response} from "express";

const bcrypt = require("bcrypt");
const crypto = require('crypto');

export default class AuthnUtil {

  private static authnService: AuthnService = AuthnService.Instance;

  private static pepper = '4DfaIclyIcniePR8ZJVFAOwryQMDK8qfsu9PI/D51hc=';


  public static generateToken(): string {
    return crypto.randomBytes(32).toString('base64url');
  }

  public static async getAuthenticatedUser(token):Promise<string> {
    return (await this.authnService.getToken(token))?.uid;
  }

  public static async hashPassword(password: string, salt: string): Promise<HashedPassword> {
    if (salt == null) {
      salt = await bcrypt.genSalt();
    }

    return {
      hashedPassword: await bcrypt.hash((this.pepper + password), salt),
      salt: salt
    }
  }

  public static async checkForPermissions(req: Request, res: Response): Promise<boolean> {
    try {
      const token = this.getTokenFromAuthHeader(req.headers.authorization);
      const userId: string = await AuthnUtil.getAuthenticatedUser(token);
      if (userId != null && userId == req.params.uid) {
        return true;
      }
    } catch (e) {
      //goes to response 403
    }
    res.status(403).json({error: "You do not have permission to access the resource"});
    return false;
  }

  public static getTokenFromAuthHeader(header: string) {
    return header.substr("Bearer ".length);
  }

}

export interface HashedPassword {
  hashedPassword: string,
  salt: string
}
