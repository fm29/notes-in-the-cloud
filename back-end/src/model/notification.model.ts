import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const NotificationSchema = new Schema({
  _id: Schema.Types.ObjectId,
  uid: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  dueDate: {
    type: String,
    required: true
  }
}, {
  versionKey: false
})

NotificationSchema.index({uid: 1, _id: 1}, {background: true})

export const Notification = mongoose.model('notification', NotificationSchema);
