import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const NoteSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true
  },
  uid: {
    type: String,
    required: true
  },
  content: {
    type: String
  },
  authorizedUsers: [{
    type: String,
    unique: true
  }]
}, {
  versionKey: false
});

NoteSchema.index({uid: 1, _id: 1}, {background: true})

export const Note = mongoose.model('note', NoteSchema);

