import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserSchema = new Schema({
    _id: Schema.Types.ObjectId,
    username: {
      type: String,
      required: true,
      unique: true,
      index: 1
    },
    hashedPassword: {
      type: String,
      required: true
    },
    salt: {
      type: String,
      required: true
    },
    email: {
      type: String
    },
    firstName: {
      type: String
    },
    lastName: {
      type: String
    },
    gender: {
      type: String
    }
  },
  {
    _id: false,
    versionKey: false
  }
);

export const User = mongoose.model('user', UserSchema);
