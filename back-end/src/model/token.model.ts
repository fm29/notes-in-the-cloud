import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const TokenSchema = new Schema({
  uid: {
    type: String,
    required: true
  },
  token: {
    type: String,
    unique: true,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now(),
    expires: 60 * 60 * 12 //12 hours
  }
}, {
  versionKey: false
});

TokenSchema.index({token: 1, uid: 1}, {background: true});


export const Token = mongoose.model('token', TokenSchema);

