import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const ToDoSchema = new Schema({
    uid: {
      type: String,
      required: true,
      unique: true
    },
    toDoItems: [{
      type: String
    }]
  },
  {
    versionKey: false
  }
);

export const ToDo = mongoose.model('todo', ToDoSchema);
