import { Selector } from "testcafe";

fixture `Login Test`
.page('http://localhost:4200')

test("User cannot login with invalid credentials", async t => {

  const loginForm = Selector('#login-form')
  await t.expect(loginForm.exists).ok()

  const userNameInput = Selector('#user-login')
  const passwordInput = Selector('#user-password')
  await t.typeText(userNameInput, 'username', {paste: true})
  await t.typeText(passwordInput, "password", {paste: true})

  const loginButton = Selector('#login-button')
  await t.expect(loginButton.exists).ok()
  await t.click(loginButton())
})

test("User can login to application", async t => {

  const loginForm = Selector('#login-form')
  await t.expect(loginForm.exists).ok()

  const userNameInput = Selector('#user-login')
  const passwordInput = Selector('#user-password')
  await t.typeText(userNameInput, 'tester', {paste: true})
  await t.typeText(passwordInput, "password", {paste: true})

  const loginButton = Selector('#login-button')
  await t.expect(loginButton.exists).ok()
  await t.click(loginButton())
})
