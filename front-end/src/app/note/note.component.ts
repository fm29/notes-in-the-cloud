import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../services";
import {NoteService} from "../services/note.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Note} from "../models";

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  private note: Note;
  private isEditable: boolean = false;
  private authorizedUsers: Set<string> = new Set();
  public userInputValue: string = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private noteService: NoteService
  ) {
  }

  get getNote(): Note {
    return this.note;
  }

  get getAuthorizedUsers(): Set<string> {
    return this.authorizedUsers;
  }

  get isNoteEditable(): boolean {
    return this.isEditable;
  }

  ngOnInit(): void {
    this.readNoteFromDb();
  }

  saveNote(): void {
    this.noteService.updateNote(this.note).subscribe({
      next: response => {
        if (response.status == 200) {

        }
      }
    })
  }

  deleteNote(): void {
    this.noteService.deleteNote(this.note._id).subscribe({
      next: response => {
        this.router.navigate(["/"]);
      }
    });
  }

  addUserToAuthorizedUsersList() {
    if (this.userInputValue != "") {
      this.authorizedUsers.add(this.userInputValue);
    }
    this.userInputValue = "";
  }

  updateNoteAuthz() {
    this.noteService.updateNoteAuthz(this.note._id, this.authorizedUsers).subscribe({
      complete: () => {
        this.readNoteFromDb();
      }
    })
  }

  removeUserFromNoteAuthorizations(username: string) {
    this.authorizedUsers.delete(username);
  }

  private readNoteFromDb() {
    this.noteService.getNote(this.route.snapshot.paramMap.get('noteId')).subscribe({
      next: body => {
        this.note = body.note;
        this.isEditable = body.canEdit;
        if (this.isEditable) {
          this.authorizedUsers = new Set<string>(body.note.authorizedUsers);
        }
      },
      error: err => {
        console.log(err);
      }
    });
  }


}
