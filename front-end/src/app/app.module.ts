import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {HomeModule} from "./home/home.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "./shared/material/material.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {LoginComponent} from "./login";
import {HeaderComponent} from "./shared/header/header.component";
import { ProfileInfoComponent } from './profile-info/profile-info.component';
import { RegisterComponent } from './register';
import {appRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { AlertComponent } from './alert';
import { ErrorInterceptor, TokenInterceptor} from "./additives";
import { NoteComponent } from './note/note.component';
import {NotificationsComponent} from "./notifications/notifications.component";
import {NotificationModalComponent} from "./modals/notification-modal/notification-modal.component";
import {MatDialogModule} from "@angular/material/dialog";
import {NgbDatepickerModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToDoComponent} from "./to-do/to-do.component";
import { SearchNotesComponent } from './search-notes/search-notes.component';
import {SharedModule} from "./shared/shared.module";

@NgModule({
  imports: [
    BrowserModule,
    HomeModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    appRoutingModule,
    FormsModule,
    MatDialogModule,
    NgbModule,
    NgbDatepickerModule,
    SharedModule,
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    AlertComponent,
    ProfileInfoComponent,
    NoteComponent,
    NotificationsComponent,
    NotificationModalComponent,
    ToDoComponent,
    SearchNotesComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
