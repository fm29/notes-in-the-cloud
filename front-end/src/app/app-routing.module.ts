import {RouterModule, Routes} from "@angular/router";
import {RegisterComponent} from "./register";
import {AuthGuard} from "./additives";
import {ProfileInfoComponent} from "./profile-info/profile-info.component";
import {LoginComponent} from "./login";
import {HomeComponent} from "./home";
import {NoteComponent} from "./note/note.component";
import {NotificationsComponent} from "./notifications/notifications.component";
import {SearchNotesComponent} from "./search-notes/search-notes.component";

const routes: Routes = [
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'shared', component: HomeComponent, canActivate: [AuthGuard]},
  {path: 'profile', component: ProfileInfoComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'notes/:noteId', component: NoteComponent},
  {path: 'notifications', component: NotificationsComponent},
  {path: 'search', component: SearchNotesComponent},

  // otherwise, redirect to home
  {path: '**', redirectTo: ''}
];

export const appRoutingModule = RouterModule.forRoot(routes);
