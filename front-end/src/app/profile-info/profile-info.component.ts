import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthenticationService, UserService} from "../services";
import {Router} from "@angular/router";

@Component({
  selector: 'app-profile-info',
  templateUrl: './profile-info.component.html',
  styleUrls: ['./profile-info.component.css']
})
export class ProfileInfoComponent implements OnInit {
  profilePageForm: FormGroup;
  profilePageEditForm: FormGroup;
  public editMode: boolean = false;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.userService.getCurrentUser().subscribe(res => {
        this.profilePageForm = this.formBuilder.group({
          firstName: res.firstName || null,
          lastName: res.lastName || null,
          password: null,
          confirmPassword: null,
          email: [res.email || null, [Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
          gender: res.gender || null,
          username: res.username || null
        });
      }
    );
  }

  enterEditMode(): void {
    this.profilePageEditForm = this.formBuilder.group({
      firstName: this.profilePageForm.value.firstName,
      lastName: this.profilePageForm.value.lastName,
      phoneNumber: this.profilePageForm.value.phoneNumber,
      password: '',
      confirmPassword: '',
      email: [this.profilePageForm.value.email, [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      gender: this.profilePageForm.value.gender,
      username: this.profilePageForm.value.username
    });
    this.editMode = true;
  }

  onSubmit(): void {
    if (this.getProfileForm.invalid) {
      return;
    }
    this.editMode = false;
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.profilePageForm.value));
  }

  cancelEditMode(): void {
    this.editMode = false;
    this.profilePageEditForm = null;
  }

  saveUser(): void {
    if (this.profilePageEditForm.invalid) {
      return;
    }

    Object.getOwnPropertyNames(this.profilePageEditForm.value).forEach(property => {
      if (this.profilePageEditForm.value[property] == null || this.profilePageEditForm.value[property].trim() == '') {
        this.profilePageEditForm.value[property] = undefined;
      }
    });
    this.userService.updateCurrentUser(JSON.stringify(this.profilePageEditForm.value)).subscribe(response => {
      if (response.status == 200) {
        this.profilePageForm = this.profilePageEditForm;
        this.profilePageEditForm = null;
        this.profilePageForm.value.password = "";
        this.profilePageForm.value.confirmPassword = "";
        this.editMode = false;
      } else {
        console.log(response.body.error);
      }
    });
  }

  get getProfileForm(): FormGroup {
    return this.editMode ? this.profilePageEditForm : this.profilePageForm;
  }

  isEditMode(): boolean {
    return this.editMode;
  }

  deleteUser() {
    this.userService.deleteCurrentUser().subscribe({
      complete: () => {
        this.authenticationService.deleteTokenAndUserIdCookies();
        this.router.navigate(['/login']);
      }
    })
  }

}
