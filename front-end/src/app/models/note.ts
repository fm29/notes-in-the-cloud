export class Note {
  _id: string;
  uid: string;
  name: string;
  content: string;
  authorizedUsers: string[];
}
