export class Notification {
  _id: string;
  uid: string;
  dueDate: string;
  message: string;
}
