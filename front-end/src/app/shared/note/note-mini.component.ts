import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-mini-note',
  templateUrl: './note-mini.component.html',
  styleUrls: ['./note-mini.component.css']
})
export class NoteMiniComponent implements OnInit {

  @Input()
  noteId: string = "";

  @Input()
  noteTitle: string = "Note title";

  @Input()
  noteContent: string = "";

  constructor() { }

  ngOnInit(): void {
  }

}
