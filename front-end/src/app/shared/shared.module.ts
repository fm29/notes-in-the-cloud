import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoteMiniComponent } from './note/note-mini.component';
import {MaterialModule} from "./material/material.module";
import {RouterModule} from "@angular/router";



@NgModule({
  declarations: [
    NoteMiniComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ],
  exports: [
    NoteMiniComponent
  ]
})
export class SharedModule { }
