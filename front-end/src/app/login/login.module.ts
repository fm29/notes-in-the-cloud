import {RouterModule} from "@angular/router";
import {SharedModule} from "../shared/shared.module";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {LoginComponent} from "./login.component";

@NgModule({
  imports: [SharedModule, MatButtonModule, MatCheckboxModule],
  declarations: [],
  exports: []
})
export class LoginModule {}
