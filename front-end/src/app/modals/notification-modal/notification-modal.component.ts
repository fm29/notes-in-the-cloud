import {Component} from "@angular/core";
import {NgbActiveModal, NgbCalendar, NgbDateStruct, NgbTimeStruct} from "@ng-bootstrap/ng-bootstrap";
import {NotificationsService} from "../../notifications/notifications.service";
import {Notification} from "../../models/notification";

@Component({
  selector: 'notification-modal',
  templateUrl: './notification-modal.component.html',
  styleUrls: ['./notification-modal.component.css']
})
export class NotificationModalComponent {
  model: NgbDateStruct;
  date: { year: number, month: number };
  time: NgbTimeStruct = {hour: 13, minute: 30, second: 30};
  seconds = true;
  notificationMessage: string;

  constructor(public activeModal: NgbActiveModal,
              private calendar: NgbCalendar,
              private notificationService: NotificationsService) {
  }

  public createNotification() {
    const newNotification = new Notification();
    newNotification.dueDate = this.getDate();
    newNotification.message = this.notificationMessage;

    this.notificationService.createNotification(newNotification).subscribe(res => {
      this.notificationService.getCurrentUserNotifications().subscribe(response => {
        this.notificationService.setNotificationList(response);
        window.location.reload();
      })
    });
  }

  private getDate() {
    return this.model.year.toString() + '-'
      + this.model.month.toString() + '-'
      + this.model.day.toString() + ':'
      + this.time.hour.toString() + ':'
      + this.time.minute.toString() + ':'
      + this.time.second.toString();
  }
}
