import {Component, OnInit} from "@angular/core";
import {NotificationsService} from "./notifications.service";
import {NotificationModalComponent} from "../modals/notification-modal/notification-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'notification-component',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  notificationList;

  constructor(private notificationsService: NotificationsService, private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.notificationsService.getCurrentUserNotifications().subscribe(res => {
      this.notificationsService.setNotificationList(res);
      this.notificationList = res;
    });
  }

  public deleteNotificationItem(notId: string): void {
    this.notificationsService.deleteNotification(notId).subscribe(res => {
      this.notificationsService.getCurrentUserNotifications().subscribe(res => {
        this.notificationsService.setNotificationList(res);
        this.notificationList = res;
      });
    });
  }

  get getNotifications() {
    return this.notificationList;
  }

  public openCreateNotificationModal(): void {
    this.modalService.open(NotificationModalComponent, {size: 'md', backdrop: 'static'});
  }

}
