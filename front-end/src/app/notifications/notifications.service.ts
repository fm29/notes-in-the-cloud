import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "../services";
import {BehaviorSubject, Observable} from "rxjs";
import {environment} from "../../environments/environment.prod";
import {Notification} from "../models/notification";

@Injectable({providedIn: 'root'})
export class NotificationsService {
  private notificationList = new BehaviorSubject<Notification[]>(null);

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {}

  public setNotificationList(newNotificationList: Notification[]): void {
    this.notificationList.next(newNotificationList);
  }

  public getNotificationList():Observable<Notification[]> {
    return this.notificationList.asObservable();
  }

  getCurrentUserNotifications(): Observable<Notification[]> {
    return this.http.get<Notification[]>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notifications`);
  }

  deleteNotification(notId: string) {
    return this.http.delete<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notifications/${notId}`, {observe:'response'});
  }

  createNotification(notification: Notification): Observable<Notification> {
    return this.http.post<Notification>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notifications`, notification);
  }
}
