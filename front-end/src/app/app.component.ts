import {Component, ElementRef, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "./services";
import {NoteService} from "./services/note.service";
import {Note} from "./models";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  noteName: string;
  title: string = 'front-end';

  @ViewChild('closeModal')
  closeModal: ElementRef

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private noteService: NoteService
  ) {
  }

  get getAuthenticationService(): AuthenticationService {
    return this.authenticationService;
  }

  createNote() {
    const note = new Note();
    note.name = this.noteName;
    this.noteService.createNote(note).subscribe({
      next: note => {
        this.closeModal.nativeElement.click();

        this.router.navigate([`/notes/${note._id}`], {
          state: {
            note: JSON.stringify(note)
          }
        });
        this.noteName = "";
      },
      complete: ()=>{

      }
    });
  }
}
