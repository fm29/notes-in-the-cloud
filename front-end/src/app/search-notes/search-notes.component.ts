import {Component, OnInit} from '@angular/core';
import {Note} from "../models";
import {NoteService} from "../services/note.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-search-notes',
  templateUrl: './search-notes.component.html',
  styleUrls: ['./search-notes.component.css']
})
export class SearchNotesComponent implements OnInit {

  private foundNotes: Note[] = [];
  private searchText: string = "";

  constructor(
    private route: ActivatedRoute,
    private noteService: NoteService
  ) {
  }

  ngOnInit(): void {
    this.searchText = this.route.snapshot.queryParamMap.get("text")
    this.noteService.searchNotesByText(this.searchText).subscribe({
      next: response => {
        this.foundNotes = response.notes;
      }
    });

  }

  get getFoundNotes() {
    return this.foundNotes;
  }

}
