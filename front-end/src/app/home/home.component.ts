import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../services";
import {NoteService} from "../services/note.service";
import {Note} from "../models";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private notes: Note[] = [];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private noteService: NoteService
  ) {
  }

  ngOnInit(): void {
    if (this.router.url == "/") {
      this.noteService.getCurrentUserNotes().subscribe({
        next: res => {
          this.notes = res;
        }
      });
    }
    if (this.router.url == "/shared") {
      this.noteService.getSharedNotes().subscribe({
        next: res => {
          this.notes = res;
        }
      });
    }
  }

  get getNotes() {
    return this.notes;
  }

  public hasNotes(): boolean {
    return this.getNotes.length === 0;
  }

}
