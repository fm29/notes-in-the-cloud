import {Component, OnInit} from "@angular/core";
import {FormBuilder} from "@angular/forms";
import {UserService} from "../services";


@Component({
  selector: 'to-do-component',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {
  completed: boolean = false;
  taskList: string[] = [];
  newTodoForm = this.formBuilder.group({
    todoItem: ''
  })

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {
  }

  ngOnInit(): void {
    this.userService.getCurrentUserToDoList().subscribe({
      next: res => {
        this.taskList = res.toDoItems;
      }
    })
  }

  addTask() {
    const value = this.newTodoForm.value.todoItem;
    if(value === "") {
      return;
    }
    this.taskList.push(value)
    this.newTodoForm.reset();
    this.userService.updateUserToDoList(this.taskList).subscribe({
      next: res => {
        console.log(res)
      }
    });

  }

  removeTask(i: any) {
    this.taskList.splice(i, 1);
    this.userService.updateUserToDoList(this.taskList).subscribe({
      next: res => {
        console.log(res)
      }
    });
  }

  markDone(value: any) {
    value.completed = !value.completed
    value.completed === true ?
      this.taskList.push(this.taskList.splice(this.taskList.indexOf(value), 1)[0]) :
      this.taskList.unshift(this.taskList.splice(this.taskList.indexOf(value), 1)[0])
  }
}

