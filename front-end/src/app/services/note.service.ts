import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "./authentication.service";
import {Observable} from "rxjs";
import {Note} from "../models";
import {environment} from "../../environments/environment.prod";

@Injectable({providedIn: 'root'})
export class NoteService {
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {
  }

  createNote(note: Note): Observable<Note> {
    note.uid = this.authenticationService.getCurrentUserId;
    note.content = "";
    return this.http.post<Note>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notes`, note);
  }

  deleteNote(noteId: string) {
    return this.http.delete<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notes/${noteId}`);
  }

  getNote(noteId: string): Observable<{ note: Note, canEdit: boolean }> {
    return this.http.get<{ note: Note, canEdit: boolean }>(`${environment.backendURL}/notes/${noteId}`);
  }

  getCurrentUserNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notes`);
  }

  getSharedNotes(): Observable<Note[]> {
    return this.http.get<Note[]>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/sharedNotes`);
  }


  updateNote(note: Note) {
    return this.http.patch<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notes/${note._id}`, note, {observe: "response"});
  }

  updateNoteAuthz(noteId: string, users: Set<string>) {
    return this.http.put<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/notes/${noteId}/authz`, {users: Array.from(users)})
  }

  searchNotesByText(text: string): Observable<{ notes: Note[] }> {
    return this.http.get<{ notes: Note[] }>(`${environment.backendURL}/search/notes?text=${text}`);
  }

}
