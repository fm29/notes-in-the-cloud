import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from '../models';
import {Observable} from "rxjs";
import {environment} from "../../environments/environment.prod";
import {AuthenticationService} from "./authentication.service";

@Injectable({providedIn: 'root'})
export class UserService {
  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {
  }

  getCurrentUser(): Observable<User> {
    return this.http.get<User>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}`);
  }

  register(user: User) {
    return this.http.post<any>(`${environment.backendURL}/users/register`, user);
  }

  deleteCurrentUser() {
    return this.http.delete(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}`);
  }

  updateCurrentUser(userData: any) {
    return this.http.patch<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}`, userData, {observe: 'response'});
  }

  getCurrentUserToDoList() {
    return this.http.get<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/todo`)
  }

  updateUserToDoList(toDoList: any[]) {
    return this.http.put<any>(`${environment.backendURL}/users/${this.authenticationService.getCurrentUserId}/todo`, {toDoList: toDoList})
  }
}
