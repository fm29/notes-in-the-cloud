import {Injectable} from '@angular/core';

import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class AuthenticationService {

  constructor(private http: HttpClient) {
  }

  public isUserLoggedIn(): boolean {
    return (this.getSessionToken != undefined && this.getCurrentUserId != undefined);
  }

  get getCurrentUserId(): string {
    return sessionStorage.getItem('userId')
  }

  get getSessionToken(): string {
    return sessionStorage.getItem('token');
  }

  public setCurrentUserId(userId: string): void {
    sessionStorage.setItem('userId', userId);
  }

  public setSessionToken(token: string): void {
    sessionStorage.setItem('token', token);
  }

  public deleteTokenAndUserIdCookies(){
    sessionStorage.removeItem('userId');
    sessionStorage.removeItem('token');
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post<any>(`${environment.backendURL}/users/authenticate`, {username, password});
  }

  logout(): void {
    const userId = this.getCurrentUserId;
    this.http.post(`${environment.backendURL}/users/${userId}/logout`, {}).subscribe({
      complete: () => {
        this.deleteTokenAndUserIdCookies();
        location.reload();
      }
    });
  }
}
